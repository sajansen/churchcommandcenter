import logging

from flask import Flask, render_template, redirect, url_for

import settings
from modules.camera_stream import get_camera_stream_status, start_new_camera_stream_screen

logger = logging.getLogger(__name__)
logging.basicConfig(
  format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
  level=logging.DEBUG)
app = Flask(__name__)

from api import *


@app.route('/')
def index():
  return render_template("index.html")


@app.route('/camera_stream/status')
def camera_stream_status():
  is_running, message = get_camera_stream_status()
  return render_template('camera_stream-status.html',
                         output=message,
                         is_running=is_running)


@app.route('/camera_stream/start')
def camera_stream_start():
  start_new_camera_stream_screen()
  return redirect(url_for('index'))


if __name__ == '__main__':
  app.run(port=settings.PORT)
