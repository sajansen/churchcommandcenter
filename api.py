import http

from flask import jsonify

from app import app
from modules import raspberrypi, camera_stream, scene_timer, camera_control, church_command_center

API_URL = '/api/v1'


@app.route(API_URL + '/churchcommandcenter/update', methods=['GET', 'POST'])
def api_churchcommandcenter_update():
  message = church_command_center.update_codebase()
  return jsonify({
    "message": message
  })


@app.route(API_URL + '/camera-stream/status')
def api_camera_stream_status():
  try:
    is_running, message = camera_stream.get_camera_stream_status()
  except Exception as e:
    is_running = False
    message = f"Error: {e}"

  return jsonify({
    "isRunning": is_running,
    "message": message
  })


@app.route(API_URL + '/camera-stream/start', methods=['POST'])
def api_camera_stream_start():
  camera_stream.start_new_camera_stream_screen()
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/scene-timer/status')
def api_scene_timer_status():
  try:
    is_running, message = scene_timer.get_scene_timer_status()
  except Exception as e:
    is_running = False
    message = f"Error: {e}"

  return jsonify({
    "isRunning": is_running,
    "message": message
  })


@app.route(API_URL + '/scene-timer/start', methods=['POST'])
def api_scene_timer_start():
  scene_timer.start_new_scene_timer_screen()
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/raspberrypi/status')
def api_raspberry_status():
  is_running = raspberrypi.get_raspberry_status()
  return jsonify({
    "isRunning": is_running
  })


@app.route(API_URL + '/raspberrypi/reboot', methods=['POST'])
def api_raspberry_reboot():
  raspberrypi.reboot_raspberry()
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/raspberrypi/shutdown', methods=['POST'])
def api_raspberry_shutdown():
  raspberrypi.shutdown_raspberry()
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/camera/1/preset/<int:preset>', methods=['GET', 'POST'])
def api_camera_control_call_preset(preset):
  camera_control.call_preset(preset)
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/camera/1/move/<string:command>/start', methods=['GET', 'POST'])
def api_camera_control_move_start(command):
  camera_control.move_start(command)
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/camera/1/move/<string:command>/stop', methods=['GET', 'POST'])
def api_camera_control_move_stop(command):
  camera_control.move_stop(command)
  return '', http.HTTPStatus.NO_CONTENT


@app.route(API_URL + '/camera/1/move/<string:command>', methods=['GET', 'POST'])
def api_camera_control_move_toggle(command):
  camera_control.move_toggle(command)
  return '', http.HTTPStatus.NO_CONTENT
