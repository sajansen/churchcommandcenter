#!/bin/bash

SCENE_TIMER_JAR=/home/pi/obs-scene-timer.jar

echo "Stopping previous instances..."
/usr/bin/killall scenetimer
echo "Starting new instance..."
java -jar "${SCENE_TIMER_JAR}" --offline
echo "Exiting..."