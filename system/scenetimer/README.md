# Camera Stream

Copy the scene timer executor command to `/usr/local/bin`:

```bash
COMMANDO_DIRECTORY=$HOME
cp scenetimer_start.sh $COMMANDO_DIRECTORY/scenetimer_start.sh
```

Copy the camera stream service file to `/etc/systemd/system` and enable it:

```bash
# Prepare service file
cat scenetimer.service.template | sed "s#\/directorypath#${COMMANDO_DIRECTORY}#g" > scenetimer.service

# Deploy service file
sudo cp scenetimer.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/scenetimer.service

# Run service file
sudo systemctl enable scenetimer   # Run at boot
```
