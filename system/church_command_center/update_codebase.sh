#!/bin/bash

echo "Updating ChurchCommandCenter codebase script..."

BASE_DIR="${1}"
cd "${BASE_DIR}"

echo "Git fetch..."
git fetch
echo "Git pull..."
RESULT=$(git pull)

echo "------"
echo "$RESULT"
if [ "$RESULT" = "Already up to date." ]; then
  echo "Exiting script."
  exit 0
fi

echo "Update packages..."
pip install -r requirements