# Camera Stream

Copy the camera stream executor command to `/usr/local/bin`:

```bash
COMMANDO_DIRECTORY=$HOME
cp camerastream_start.sh $COMMANDO_DIRECTORY/camerastream_start.sh
```

Copy the camera stream service file to `/etc/systemd/system` and enable it:

```bash
# Prepare service file
cat camerastream.service.template | sed "s#\/directorypath#${COMMANDO_DIRECTORY}#g" > camerastream.service

# Deploy service file
sudo cp camerastream.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/camerastream.service

# Run service file
sudo systemctl enable camerastream   # Run at boot
```
