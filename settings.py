import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

PORT = 8080

CHURCH_COMMAND_CENTER_UPDATE_CODEBASE_COMMAND = BASE_DIR + "/system/church_command_center/update_codebase.sh " + BASE_DIR

CAMERA_STREAM_START_COMMAND = ""
CAMERA_STREAM_STATUS_COMMAND = ""

SCENE_TIMER_START_COMMAND = "sudo systemctl restart scenetimer"
SCENE_TIMER_STATUS_COMMAND = "sudo systemctl is-active scenetimer"

RASPBERRY_SCREEN_IDENTIFIER = "CameraStreamRaspberry"
RASPBERRY_REBOOT_COMMAND = "sudo reboot"
RASPBERRY_SHUTDOWN_COMMAND = "sudo shutdown -h now"

CAMERA_CONTROL_IP = "192.168.88.100"
CAMERA_CONTROL_USERNAME = "admin"
CAMERA_CONTROL_PASSWORD = "admin"
CAMERA_CONTROL_PRESET_URL = "http://{ip}/cgi-bin/ptz.cgi?action=start&channel=0&code=GotoPreset&arg1=0&arg2={preset}&arg3=0"
CAMERA_CONTROL_MOVEMENT_URL = "http://{ip}/cgi-bin/ptz.cgi?action={action}&channel=0&code={direction}&arg1={speed1}&arg2={speed2}&arg3=0"
CAMERA_CONTROL_MOVEMENT_DURATION = 0.2

localsettingspath = os.path.join(BASE_DIR, "settings_local.py")
if os.path.exists(localsettingspath):
  from settings_local import *
