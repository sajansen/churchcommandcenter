from waitress import serve

import app
import settings

serve(app.app, host='0.0.0.0', port=settings.PORT)
