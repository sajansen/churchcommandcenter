import logging

from modules.commandutils import execute_bash_command
import settings

logger = logging.getLogger(__name__)


def get_raspberry_status() -> bool:
  return True


def reboot_raspberry() -> str:
  logger.info("Rebooting PI")
  # Create screen named IDENTIFIER and run the INTERNAL_COMMAND inside it
  command = f"screen -S {settings.RASPBERRY_SCREEN_IDENTIFIER} -dm bash -c '{settings.RASPBERRY_REBOOT_COMMAND}'"
  output_message = execute_bash_command(command)
  logger.warning(output_message)
  return output_message


def shutdown_raspberry() -> str:
  logger.info("Shutting down PI")
  # Create screen named IDENTIFIER and run the command inside it
  command = f"screen -S {settings.RASPBERRY_SCREEN_IDENTIFIER} -dm bash -c '{settings.RASPBERRY_SHUTDOWN_COMMAND}'"
  output_message = execute_bash_command(command)
  logger.warning(output_message)
  return output_message


