import logging

from modules.commandutils import execute_bash_command
import settings

logger = logging.getLogger(__name__)


def get_scene_timer_status() -> (bool, str):
  command = settings.SCENE_TIMER_STATUS_COMMAND  # Run get status command
  output_message = execute_bash_command(command)
  if output_message is None:
    raise ValueError("Output of bash command was None")

  is_running = output_message.strip() == "active"

  return is_running, output_message


def start_new_scene_timer_screen() -> str:
  logger.info("Starting Scene Timer application")
  command = settings.SCENE_TIMER_START_COMMAND  # Run the INTERNAL_COMMAND
  output_message = execute_bash_command(command)
  logger.warning(output_message)
  return output_message
