import logging
import time

import requests
from requests.auth import HTTPBasicAuth

import settings

logger = logging.getLogger(__name__)


def call_preset(preset: int):
    url = settings.CAMERA_CONTROL_PRESET_URL.format(ip=settings.CAMERA_CONTROL_IP, preset=preset)
    logger.info("Setting camera preset: " + url)
    requests.get(url, auth=HTTPBasicAuth("admin", "admin"))


def move_toggle(command: str, speed: int = 4):
    url = get_url_for_move(command, speed)
    logger.info("Moving camera: " + url)
    requests.get(url.format(action="start"), auth=HTTPBasicAuth("admin", "admin"))
    time.sleep(settings.CAMERA_CONTROL_MOVEMENT_DURATION)
    requests.get(url.format(action="stop"), auth=HTTPBasicAuth("admin", "admin"))


def move_start(command: str, speed: int = 4):
    url = get_url_for_move(command, speed)
    logger.info("Start moving camera: " + url)
    requests.get(url.format(action="start"), auth=HTTPBasicAuth("admin", "admin"))


def move_stop(command: str, speed: int = 4):
    url = get_url_for_move(command, speed)
    logger.info("Stop moving camera: " + url)
    requests.get(url.format(action="stop"), auth=HTTPBasicAuth("admin", "admin"))


def get_url_for_move(command: str, speed: int = 4) -> str:
    speed1 = 0
    speed2 = speed

    directions = [
        "LeftDown",
        "Down",
        "RightDown",
        "Left",
        "PositionReset",
        "Right",
        "LeftUp",
        "Up",
        "RightUp",
    ]

    if command == "+":
        direction = "ZoomTele"
    elif command == "-":
        direction = "ZoomWide"
    else:
        direction = directions[int(command) - 1]

        if direction == "PositionReset":
            speed1 = 0
            speed2 = 0
        elif direction in ["LeftDown", "RightDown", "LeftUp", "RightUp"]:
            speed1 = speed

    return settings.CAMERA_CONTROL_MOVEMENT_URL.format(ip=settings.CAMERA_CONTROL_IP, direction=direction,
                                                       speed1=speed1, speed2=speed2,
                                                       action="{action}")
