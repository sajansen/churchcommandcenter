import logging
import shlex
import subprocess

logger = logging.getLogger(__name__)


def execute_bash_command(command: str = None) -> str:
  if command is None:
    raise ValueError("No argument command given: command was None")

  logger.info("Executing bash command: %s", command)
  process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, shell=False)
  output, error = process.communicate()

  logger.debug("Bash command output: %s", output)
  return output.decode() if output is not None else None
