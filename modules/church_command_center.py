import logging

from modules.commandutils import execute_bash_command
import settings

logger = logging.getLogger(__name__)


def update_codebase() -> str:
  logger.info("Updating codebase Church Command Center")
  command = settings.CHURCH_COMMAND_CENTER_UPDATE_CODEBASE_COMMAND
  output_message = execute_bash_command(command)
  logger.warning(output_message)
  return output_message


