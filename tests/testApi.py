import http
import json
import unittest

import api
from app import app
from tests.utils import raise_


class ApiTest(unittest.TestCase):

  def setUp(self) -> None:
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    self.app = app.test_client()

  def testCameraStreamApiWithNotRunningStatus(self):
    api.camera_stream.get_camera_stream_status = lambda: (False, "My message")
    expected_dict = {
      "isRunning": False,
      "message": "My message"
    }

    response = self.app.get('/api/v1/camera-stream/status', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.OK, response.status_code)
    self.assertEqual(expected_dict, json.loads(response.data.decode()))

  def testCameraStreamApiWithRunningStatus(self):
    api.camera_stream.get_camera_stream_status = lambda: (True, "My message")
    expected_dict = {
      "isRunning": True,
      "message": "My message"
    }

    response = self.app.get('/api/v1/camera-stream/status', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.OK, response.status_code)
    self.assertEqual(expected_dict, json.loads(response.data.decode()))

  def testCameraStreamApiWithExceptionRaised(self):
    api.camera_stream.get_camera_stream_status = lambda: raise_(ValueError("Blabla"))
    expected_dict = {
      "isRunning": False,
      "message": "Error: Blabla"
    }

    response = self.app.get('/api/v1/camera-stream/status', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.OK, response.status_code)
    self.assertEqual(expected_dict, json.loads(response.data.decode()))

  def testCameraStreamApiStart(self):
    api.camera_stream.start_new_camera_stream_screen = lambda: None

    response = self.app.post('/api/v1/camera-stream/start', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.NO_CONTENT, response.status_code)

  def testRaspberryPIApiWithNotRunningStatus(self):
    api.raspberrypi.get_raspberry_status = lambda: False
    expected_dict = {
      "isRunning": False,
    }

    response = self.app.get('/api/v1/raspberrypi/status', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.OK, response.status_code)
    self.assertEqual(expected_dict, json.loads(response.data.decode()))

  def testRaspberryPIApiWithRunningStatus(self):
    api.raspberrypi.get_raspberry_status = lambda: True
    expected_dict = {
      "isRunning": True,
    }

    response = self.app.get('/api/v1/raspberrypi/status', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.OK, response.status_code)
    self.assertEqual(expected_dict, json.loads(response.data.decode()))

  def testRaspberryPIApiReboot(self):
    api.raspberrypi.reboot_raspberry = lambda: None

    response = self.app.post('/api/v1/raspberrypi/reboot', follow_redirects=True)

    self.assertEqual(http.HTTPStatus.NO_CONTENT, response.status_code)


if __name__ == "__main__":
  unittest.main()
