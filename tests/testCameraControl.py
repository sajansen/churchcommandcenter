import unittest

import settings
from modules import camera_control


class CameraControlTest(unittest.TestCase):

    def setUp(self) -> None:
        settings.CAMERA_CONTROL_IP = "192.168.178.100"

    def testCameraMovementLeftDown(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=LeftDown&arg1=4&arg2=4&arg3=0",
            camera_control.get_url_for_move("1"))

    def testCameraMovementDown(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=Down&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("2"))

    def testCameraMovementRightDown(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=RightDown&arg1=4&arg2=4&arg3=0",
            camera_control.get_url_for_move("3"))

    def testCameraMovementLeft(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=Left&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("4"))

    def testCameraMovementPositionReset(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=PositionReset&arg1=0&arg2=0&arg3=0",
            camera_control.get_url_for_move("5"))

    def testCameraMovementRight(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=Right&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("6"))

    def testCameraMovementLeftUp(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=LeftUp&arg1=4&arg2=4&arg3=0",
            camera_control.get_url_for_move("7"))

    def testCameraMovementUp(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=Up&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("8"))

    def testCameraMovementRightUp(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=RightUp&arg1=4&arg2=4&arg3=0",
            camera_control.get_url_for_move("9"))

    def testCameraMovementZoomTele(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=ZoomTele&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("+"))

    def testCameraMovementZoomWide(self):
        self.assertEqual(
            "http://192.168.178.100/cgi-bin/ptz.cgi?action={action}&channel=0&code=ZoomWide&arg1=0&arg2=4&arg3=0",
            camera_control.get_url_for_move("-"))


if __name__ == "__main__":
    unittest.main()
