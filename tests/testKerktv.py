import unittest

import settings
from app import app
from modules import camera_stream


class CameraStreamTest(unittest.TestCase):

  def setUp(self) -> None:
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    self.app = app.test_client()

    camera_stream.execute_bash_command = lambda x: x  # Return input command as output
    settings.CAMERA_STREAM_START_COMMAND = "internal_command"

  def testCameraStreamStatusIsNotRunning(self):
    camera_stream.execute_bash_command = lambda x: "inactive"

    is_running, output_message = camera_stream.get_camera_stream_status()

    self.assertEqual(False, is_running)
    self.assertEqual("inactive", output_message)

  def testCameraStreamStatusIsRunning(self):
    camera_stream.execute_bash_command = lambda x: "active"

    is_running, output_message = camera_stream.get_camera_stream_status()

    self.assertEqual(True, is_running)
    self.assertEqual("active", output_message)

  def testCameraStreamStatusReturnsNone(self):
    camera_stream.execute_bash_command = lambda x: None

    self.assertRaises(ValueError, camera_stream.get_camera_stream_status)

  def testCameraStreamStart(self):
    output_message = camera_stream.start_new_camera_stream_screen()

    self.assertIn("internal_command", output_message)


if __name__ == "__main__":
  unittest.main()
