import Coordinate from "~/js/components/Coordinate";

const assert = require('chai').assert;
const expect = require('chai').expect;

describe("Coordinate", function () {
    it("should be inited from two values and return this values when asked", function () {
        const coordinate = new Coordinate(12, 33);

        assert.equal(coordinate.x, 12);
        assert.equal(coordinate.y, 33);
    });
});
