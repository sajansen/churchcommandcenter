import Coordinate from "~/js/components/Coordinate";

export default class SlideButton {
    /**
     * Make a DOM element interactive by sliding it horizontal
     * @param element   The target DOM element (or ID string)
     * @param elementParent     The container wherein the target element must be contained (or ID string)
     * @param actionHandler     The action to execute when the element is fully slided to the left
     */
    constructor(element, elementParent, actionHandler) {
        if (typeof element == 'string')
            element = document.getElementById(element);
        if (typeof elementParent == 'string')
            elementParent = document.getElementById(elementParent);

        this.element = element;
        this.parent = elementParent;
        this.actionHandler = actionHandler;
        this.dropZoneMargin = 5;

        this.element.addEventListener('mousedown', this.dragEventStart.bind(this));
        this.element.addEventListener('touchstart', this.dragEventStart.bind(this));
        window.addEventListener('mousemove', this.dragEventMove.bind(this));
        window.addEventListener('touchmove', this.dragEventMove.bind(this));
        window.addEventListener('mouseup', this.dragEventStop.bind(this));
        window.addEventListener('touchend', this.dragEventStop.bind(this));
        window.addEventListener('touchcancel', this.dragEventStop.bind(this));
    }

    dragEventStart(event) {
        this.isDragging = true;

        this.mouseStartCoordinate = this.getClickOrTouchCoordinates(event);
        this.elementStartStyleLeft = parseInt(this.element.style.left) | 0;

        this.elementStartBoundaries = this.element.getBoundingClientRect();
        this.parentBoundaries = this.parent.getBoundingClientRect();
    };

    dragEventMove(event) {
        if (!this.isDragging) return;

        const mouseCoordinates = this.getClickOrTouchCoordinates(event);
        let left = mouseCoordinates.x - this.mouseStartCoordinate.x;

        if (this.elementStartBoundaries.left + left < this.parentBoundaries.left) {
            left = this.parentBoundaries.left - this.elementStartBoundaries.left;
        } else if (this.elementStartBoundaries.right + left > this.parentBoundaries.right) {
            left = this.parentBoundaries.right - this.elementStartBoundaries.right;
        }

        this.element.style.left = this.elementStartStyleLeft + left + "px";
    }

    dragEventStop(event) {
        if (!this.isDragging) return;
        this.isDragging = false;

        // Pull to drop zone or pull back to start
        const elementStopBoundaries = this.element.getBoundingClientRect();
        if (elementStopBoundaries.left > this.parentBoundaries.left + this.dropZoneMargin) {
            this.element.style.left = this.elementStartStyleLeft + "px";
            return;
        }
        this.element.style.left = this.parentBoundaries.left - this.elementStartBoundaries.left + "px";

        // Execute action and reset element
        this.actionHandler(event);
        this.element.style.left = this.elementStartStyleLeft + "px";
    }

    // Be compatible with cursor clicks and touch clicks
    getClickOrTouchCoordinates(event) {
        if (event.touches != null && event.touches.length > 0) {
            return new Coordinate(event.touches[0].clientX, event.touches[0].clientY);
        } else {
            return new Coordinate(event.clientX, event.clientY);
        }
    }
}