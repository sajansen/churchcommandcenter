
export default class Coordinate {
  constructor(x = null, y = null){
    this.x = x;
    this.y = y;
  }

  toArray() {
    return [this.x, this.y];
  }
}