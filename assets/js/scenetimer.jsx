import {api} from "./api";
import {CommandStatus, updateCommandStatusComponent} from "~/js/commandutils";
import SlideButton from "~/js/components/SlideButton";

export const statusElement = document.getElementById("command-scene-timer-status");
const actionStartElement = document.getElementById("command-scene-timer-action-start");
const loadIconElement = document.getElementById("scene-timer-load-icon");
const errorIconElement = document.getElementById("scene-timer-error-icon");

export const setupGetSceneTimerStatus = () => {
    getSceneTimerStatus();
    setInterval(() => getSceneTimerStatus(), 5000);
};

const getSceneTimerStatus = () => {
    errorIconElement.style.display = "none";
    loadIconElement.style.display = "block";

    api.scene_timer.status()
       .catch((resp) => {
           console.error("Error getting SceneTimer status");
           console.error(resp);
           errorIconElement.style.display = "block";
           updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);
       })
       .then((data) => {
           if (data === undefined) return;
           if (!data.data) {
               console.error("Response object is not valid. Missing 'data' attribute:");
               console.error(data);
               errorIconElement.style.display = "block";
               return;
           }

           updateCommandStatusComponent(statusElement, data.data.isRunning ? CommandStatus.RUNNING : CommandStatus.NOT_RUNNING);
           errorIconElement.style.display = "none";
       })
       .done(() => {
           loadIconElement.style.display = "none";
       });
};

export const setupSceneTimerActionStart = () => {
    const actionStartParentElement = actionStartElement.closest('.command-section');
    new SlideButton(actionStartElement, actionStartParentElement, startSceneTimer);
};

const startSceneTimer = (event) => {
    console.debug("Starting SceneTimer");
    actionStartElement.disabled = true;
    updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);

    api.scene_timer.start()
       .catch((resp) => {
           console.error("Error starting SceneTimer");
           console.error(resp);
       })
       .done(() => {
           actionStartElement.disabled = false;
           getSceneTimerStatus();
       });
};
