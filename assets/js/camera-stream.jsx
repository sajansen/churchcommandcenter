import {api} from "./api";
import {CommandStatus, updateCommandStatusComponent} from "~/js/commandutils";
import SlideButton from "~/js/components/SlideButton";

export const statusElement = document.getElementById("command-camera-stream-status");
const actionStartElement = document.getElementById("command-camera-stream-action-start");
const loadIconElement = document.getElementById("camera-stream-load-icon");
const errorIconElement = document.getElementById("camera-stream-error-icon");

export const setupGetCameraStreamStatus = () => {
    getCameraStreamStatus();
    setInterval(() => getCameraStreamStatus(), 3000);
};

const getCameraStreamStatus = () => {
    errorIconElement.style.display = "none";
    loadIconElement.style.display = "block";

    api.camera_stream.status()
       .catch((resp) => {
           console.error("Error getting CameraStream Camera status");
           console.error(resp);
           errorIconElement.style.display = "block";
           updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);
       })
       .then((data) => {
           if (data === undefined) return;
           if (!data.data) {
               console.error("Response object is not valid. Missing 'data' attribute:");
               console.error(data);
               errorIconElement.style.display = "block";
               return;
           }

           updateCommandStatusComponent(statusElement, data.data.isRunning ? CommandStatus.RUNNING : CommandStatus.NOT_RUNNING);
           errorIconElement.style.display = "none";
       })
       .done(() => {
           loadIconElement.style.display = "none";
       });
};

export const setupCameraStreamActionStart = () => {
    const actionStartParentElement = actionStartElement.closest('.command-section');
    new SlideButton(actionStartElement, actionStartParentElement, startCameraStream);
};

const startCameraStream = (event) => {
    console.debug("Starting CameraStream Camera");
    actionStartElement.disabled = true;
    updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);

    api.camera_stream.start()
       .catch((resp) => {
           console.error("Error starting CameraStream Camera");
           console.error(resp);
       })
       .done(() => {
           actionStartElement.disabled = false;
           getCameraStreamStatus();
       });
};
