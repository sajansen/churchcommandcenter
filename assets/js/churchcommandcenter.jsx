import {api} from "./api";
import {CommandStatus, updateCommandStatusComponent} from "~/js/commandutils";
import SlideButton from "~/js/components/SlideButton";

export const statusElement = document.getElementById("command-ccc-update-status");
const actionStartUpdateElement = document.getElementById("command-ccc-update");
const loadIconElement = document.getElementById("ccc-load-icon");
const errorIconElement = document.getElementById("ccc-error-icon");

const getChurchCommandCenterStatus = () => {
    errorIconElement.style.display = "none";
    loadIconElement.style.display = "block";

    api.raspberrypi.status()
       .catch((resp) => {
           console.error("Error getting RaspberryPI status");
           console.error(resp);
           errorIconElement.style.display = "block";
           updateCommandStatusComponent(statusElement, CommandStatus.NOT_RUNNING);
       })
       .then((data) => {
           if (data === undefined) return;
           if (!data.data) {
               console.error("Response object is not valid. Missing 'data' attribute:");
               console.error(data);
               errorIconElement.style.display = "block";

               updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);
               return;
           }

           updateCommandStatusComponent(statusElement, data.data.isRunning ? CommandStatus.RUNNING : CommandStatus.NOT_RUNNING);
           errorIconElement.style.display = "none";
       })
       .done(() => {
           loadIconElement.style.display = "none";
       });
};

export const setupChurchCommandCenterUpdateCodebase = () => {
    const actionStartParentElement = actionStartUpdateElement.closest('.command-section');
    new SlideButton(actionStartUpdateElement, actionStartParentElement, updateCodebase);
    updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);
};

const updateCodebase = (event) => {
    console.debug("Updating ChurchCommandCenter");
    actionStartUpdateElement.disabled = true;

    updateCommandStatusComponent(statusElement, CommandStatus.RUNNING);
    errorIconElement.style.display = "none";
    loadIconElement.style.display = "block";

    api.church_command_center.update()
       .catch((resp) => {
           console.error("Error updating ChurchCommandCenter");
           console.error(resp);

           actionStartUpdateElement.disabled = false;

           updateCommandStatusComponent(statusElement, CommandStatus.ERROR);
           errorIconElement.style.display = "block";
           loadIconElement.style.display = "none";
       })
       .done(() => {
           actionStartUpdateElement.disabled = false;

           updateCommandStatusComponent(statusElement, CommandStatus.DONE);
           errorIconElement.style.display = "none";
           loadIconElement.style.display = "none";
       });
};
