export const CommandStatus = Object.freeze({
    UNKNOWN: 0,
    STARTING: 1,
    RUNNING: 2,
    DONE: 3,
    FAILED: 4,
    ERROR: 5,
    NOT_RUNNING: 6,
    SUCCESS: 7,
});

export function updateCommandStatusComponent(statusElement, commandStatus) {
    if (statusElement instanceof NodeList) {
        statusElement.forEach((element) => updateCommandStatusComponent(element, commandStatus))
        return
    }

    removeCommandStatusClasses(statusElement);

    let commandStatusString = Object.keys(CommandStatus)[commandStatus];
    statusElement.innerText = commandStatusString.replace('_', ' ');
    statusElement.classList.add('command-status-' + commandStatusString.toLowerCase());
    statusElement.setAttribute('status', commandStatusString);
}

function removeCommandStatusClasses(element) {
    Object.keys(CommandStatus).forEach(key => element.classList.remove(`command-status-${key.toLowerCase()}`));
}