import {setupGetCameraStreamStatus, setupCameraStreamActionStart} from "~/js/camera-stream";
import {
      setupGetRaspberryPIStatus,
      setupRaspberryPIActionReboot,
      setupRaspberryPIActionShutdown
} from "~/js/rasbperrypi";
import {setupGetSceneTimerStatus, setupSceneTimerActionStart} from "~/js/scenetimer";
import {setupChurchCommandCenterUpdateCodebase} from "~/js/churchcommandcenter";


window.indexPage = () => {
      setupGetCameraStreamStatus();
      setupCameraStreamActionStart();

      setupGetSceneTimerStatus();
      setupSceneTimerActionStart();

      setupGetRaspberryPIStatus();
      setupRaspberryPIActionReboot();
      setupRaspberryPIActionShutdown();

      setupChurchCommandCenterUpdateCodebase();
};