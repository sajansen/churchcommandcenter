let Q = require('q-xhr');

export const apiUrl = '/api/v1';

function post(url, data, options = {}) {
    // options.headers = _.defaults(options.headers || {}, {
    //   'X-CSRFToken': Cookies.get('csrftoken')
    // });

    return Q.xhr.post(url, data, options);
}


export const api = {
    camera_stream: {
        status: () => {
            return Q.xhr.get(`${apiUrl}/camera-stream/status`);
        },
        start: () => {
            return post(`${apiUrl}/camera-stream/start`);
        },
    },
    scene_timer: {
        status: () => {
            return Q.xhr.get(`${apiUrl}/scene-timer/status`);
        },
        start: () => {
            return post(`${apiUrl}/scene-timer/start`);
        },
    },
    raspberrypi: {
        status: () => {
            return Q.xhr.get(`${apiUrl}/raspberrypi/status`);
        },
        reboot: () => {
            return post(`${apiUrl}/raspberrypi/reboot`);
        },
        shutdown: () => {
            return post(`${apiUrl}/raspberrypi/shutdown`);
        },
    },
    church_command_center: {
        update: () => {
            return post(`${apiUrl}/churchcommandcenter/update`);
        },
    },
};