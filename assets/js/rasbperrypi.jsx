import {api} from "./api";
import {CommandStatus, updateCommandStatusComponent} from "~/js/commandutils";
import SlideButton from "~/js/components/SlideButton";

const statusElement = document.querySelectorAll(".command-raspberrypi-status");
const actionRebootElement = document.getElementById("command-raspberrypi-action-reboot");
const actionShutdownElement = document.getElementById("command-raspberrypi-action-shutdown");
const loadIconElement = document.getElementById("raspberrypi-load-icon");
const errorIconElement = document.getElementById("raspberrypi-error-icon");

export const setupGetRaspberryPIStatus = () => {
    getRaspberryPIStatus();
    setInterval(() => getRaspberryPIStatus(), 10000);
};

const getRaspberryPIStatus = () => {
    errorIconElement.style.display = "none";
    loadIconElement.style.display = "block";

    api.raspberrypi.status()
       .catch((resp) => {
           console.error("Error getting RaspberryPI status");
           console.error(resp);
           errorIconElement.style.display = "block";
           updateCommandStatusComponent(statusElement, CommandStatus.NOT_RUNNING);
       })
       .then((data) => {
           if (data === undefined) return;
           if (!data.data) {
               console.error("Response object is not valid. Missing 'data' attribute:");
               console.error(data);
               errorIconElement.style.display = "block";

               updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);
               return;
           }

           updateCommandStatusComponent(statusElement, data.data.isRunning ? CommandStatus.RUNNING : CommandStatus.NOT_RUNNING);
           errorIconElement.style.display = "none";
       })
       .done(() => {
           loadIconElement.style.display = "none";
       });
};

export const setupRaspberryPIActionReboot = () => {
    const actionRebootParentElement = actionRebootElement.closest('.command-section');
    new SlideButton(actionRebootElement, actionRebootParentElement, rebootRaspberryPI);
};

const rebootRaspberryPI = (event) => {
    console.debug("Rebooting RaspberryPI");
    actionRebootElement.disabled = true;
    updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);

    api.raspberrypi.reboot()
       .catch((resp) => {
           console.error("Error rebooting RaspberryPI");
           console.error(resp);
       })
       .done(() => {
           actionRebootElement.disabled = false;
           getRaspberryPIStatus();
       });
};

export const setupRaspberryPIActionShutdown = () => {
    const actionShutdownParentElement = actionShutdownElement.closest('.command-section');
    new SlideButton(actionShutdownElement, actionShutdownParentElement, shutdownRaspberryPI);
};

const shutdownRaspberryPI = (event) => {
    console.debug("Shutting down RaspberryPI");
    actionShutdownElement.disabled = true;
    updateCommandStatusComponent(statusElement, CommandStatus.UNKNOWN);

    api.raspberrypi.shutdown()
       .catch((resp) => {
           console.error("Error shutting down RaspberryPI");
           console.error(resp);
       })
       .done(() => {
           actionShutdownElement.disabled = false;
           getRaspberryPIStatus();
       });
};
