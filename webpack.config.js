const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'production',

    entry: './assets/index.js',
    resolve: {
        modules: [
            path.resolve(__dirname, "assets/js"),
            path.resolve(__dirname, "assets/style"),
            "node_modules"
        ],
        extensions: [".jsx", ".js", ".json"],
        alias: {
            '~': path.resolve(__dirname, 'assets'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js|\.jsx$/,
                use: [
                    'babel-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(css|sass|scss)$/,
                use: ExtractTextPlugin.extract({
                                                   fallback: 'style-loader',
                                                   use: [
                                                       {
                                                           loader: 'css-loader'
                                                       },
                                                       {
                                                           loader: 'postcss-loader', // Run post css actions
                                                           options: {
                                                               plugins: function () { // post css plugins, can be exported to postcss.config.js
                                                                   return [
                                                                       // require('precss'),
                                                                       require('autoprefixer')
                                                                   ];
                                                               }
                                                           }
                                                       },
                                                       {
                                                           loader: 'sass-loader',
                                                           options: {
                                                               minimize: true
                                                           }
                                                       }
                                                   ]
                                               })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                        },
                    },
                    // 'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                }],
            }
        ]
    },
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'static')
    },

    plugins: [
        new ExtractTextPlugin("main.css"),
    ],

    performance: {
        hints: false
    }
};
