# Church Command Center

### Installation

#### Requirements

Make sure you have at least Python 3.6 installed. If not, see this how-to:https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f (or also https://raspberrypi.stackexchange.com/questions/59381/how-do-i-update-my-rpi3-to-python-3-6)


It's recommend to use a virtual environment for your projects:

```bash
python3.6 -m venv venv      # or python3.7
. venv/bin/activate         # Enter/activate the virtual environment
```

Install all dependencies (always from inside the virtualenv!):

```bash
pip install -r requirements
npm install         # You can skip from this part for production server
npm run build
```

#### Setup service

##### Camera Stream service

This is the Camera Stream extension for the Command Center. See the [readme](system/camerastream/README.md) for how to setup this service.

##### Webserver service

This is the main service to get access to the Command Center.

```bash
# Prepare service file
PROJECT_PATH=$(pwd)
cat churchcommandcenter.service.template | sed "s#\/directorypath#${PROJECT_PATH}#g" > churchcommandcenter.service

# Deploy service file
sudo cp churchcommandcenter.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/churchcommandcenter.service

# Run service file
sudo systemctl enable churchcommandcenter   # Run at boot
sudo systemctl start churchcommandcenter    # Also run right now
```

This service can be controlled by the `systemctl` command:

* `systemctl start churchcommandcenter`: Start the service
* `systemctl status churchcommandcenter`: Get the current status (logging) of the server
* `systemctl stop churchcommandcenter`: Stop the service
* `systemctl enable churchcommandcenter`: Start the service during boot
* `systemctl disable churchcommandcenter`: Stop starting the service during boot

Also, `journalctl` can be used for monitoring the logs:

`journalctl -u churchcommandcenter.service`

Add flags like `-e` for scrolling to the end or `-f` for live log following.

Complete logfile can be found in `/var/log/syslog`

#### Settings

Don't forget to configure the settings in [settings.py](settings.py), or make a copy for [settings_local.py](settings_local.py).


### Run

```bash
# Development
python -m flask run --host 0.0.0.0 --port 5000

# Production
systemctl start churchcommandcenter
# or without the service:
python waitress_ex.py
```

#### Tests
Tests can be found under:
- [tests/](tests) -> Python unit tests
- [assets/js/tests/](assets/js/tests) -> JS unit tests

### Side Notes

Some default implemented commando's are run from inside a screen session. So make sure you have `screen` installed: `sudo apt install screen`.

### Future

##### Nice to have's

* Command-status which tells you if a newer version of this software is available.
